from django.urls import path
from charityapp import views

app_name="charityapp"

urlpatterns = [
    path('about/',views.about,name="about"),
    path('contact/',views.contact,name="contact"),
    path('scope/',views.scope,name="scope"),
    path('donate/',views.donate,name="donate"),
    path('success_payment/',views.success_payment,name="success_payment"),
    path('become_volunteer/',views.become_volunteer,name="become_volunteer"),
]
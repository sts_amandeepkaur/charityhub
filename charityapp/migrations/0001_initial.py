# Generated by Django 2.1.7 on 2019-05-10 19:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='contactus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('email', models.CharField(max_length=200)),
                ('message', models.TextField()),
                ('create_date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='work',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('descripton1', models.TextField()),
                ('descripton2', models.TextField(blank=True)),
                ('descripton3', models.TextField(blank=True)),
                ('added_on', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='workImages',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image1', models.ImageField(blank=True, upload_to='works/%Y/%m/%d')),
                ('image2', models.ImageField(blank=True, upload_to='works/%Y/%m/%d')),
                ('image3', models.ImageField(blank=True, upload_to='works/%Y/%m/%d')),
                ('image4', models.ImageField(blank=True, upload_to='works/%Y/%m/%d')),
                ('added_on', models.DateField(auto_now_add=True)),
                ('work', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='charityapp.work')),
            ],
        ),
    ]

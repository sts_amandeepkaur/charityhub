from django.contrib import admin
from charityapp.models import work,workImages,contactus, donor, volunteer

class workAdmin(admin.ModelAdmin):
    list_display = ['id','title','description1','description2','description3','added_on']

class donorAdmin(admin.ModelAdmin):
    list_display = ['name','txn_id','email','country','state','city','amount','work','on_date']

class volunteerAdmin(admin.ModelAdmin):
    list_display = ['name','email','country','state','city','qualification','age','on_date']

admin.site.register(work,workAdmin)
admin.site.register(workImages)
admin.site.register(contactus)
admin.site.register(donor,donorAdmin)
admin.site.register(volunteer,volunteerAdmin)

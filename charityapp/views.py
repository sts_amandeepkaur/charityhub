from django.shortcuts import render
from charityapp.models import work, workImages, contactus, donor, volunteer
from django.http import HttpResponse, HttpResponseRedirect

works = work.objects.all().order_by('title')
images = workImages.objects.all()
volunt = volunteer.objects.filter(status=True).order_by('name')
support = []
for i in works:
    w = {}
    w['id'] = i.id
    w['title']=i.title
    w['description1']=i.description1[:120]
    support.append(w)

gallery = []
for i in images:
    gallery.append(i.image1)
    gallery.append(i.image2)
    gallery.append(i.image3)
    gallery.append(i.image4)
    

def index(request):
    return render(request,'index.html',{'works':works,'support':support,'gallery':gallery,'vol':volunt})
def about(request):
    return render(request,'about.html',{'works':works,'gallery':gallery})
def contact(request):
    if request.method=="POST":
        name = request.POST['name']
        em = request.POST['email']
        msz = request.POST['message']
        data = contactus(name=name,email=em,message=msz)
        data.save()
        return render(request,'contact.html',{'works':works,'status':'Thanks for your feedback!!!'})
    return render(request,'contact.html',{'works':works})

def scope(request):
    id = request.GET['id']
    data = work.objects.get(pk=id)
    images = workImages.objects.all()
    return render(request,'scope.html',{'works':works,'work':data,'images':images})

def donate(request):
    if 'name' in request.GET:
        name = request.GET['name']
        em = request.GET['email']
        cont = request.GET['cont']
        coun = request.GET['coun']
        st = request.GET['st']
        ct = request.GET['ct']
        add1 = request.GET['add1']
        add2 = request.GET['add2']
        amt = request.GET['amt']
        rem = request.GET['rem']
        work = request.GET['work']

        data = donor(name=name,email=em,contact=cont,country=coun,state=st,city=ct,address_line1=add1,address_line2=add2,amount=amt,remarks=rem,work=work)
        data.save()
        return HttpResponse(data.id)
    return render(request,'donate.html',{'works':works})

def success_payment(request):
    if 'ORDERID' in request.GET:
        status = request.GET['STATUS']
        if status == "TXN_FAILURE":
            return HttpResponseRedirect('/charityapp/donate/')
        order_id = request.GET['ORDERID']
        txn_id = request.GET['TXNID']
        pay_mode = request.GET['PAYMENTMODE']
        bank_name = request.GET['BANKNAME']

        sid = order_id.split('o')[0]
        order_obj = donor.objects.get(id=int(sid))
        order_obj.txn_id = txn_id
        order_obj.payment_method= pay_mode
        order_obj.bank_name = bank_name
        order_obj.status = True
        order_obj.save()

        return render(request,'process.html',{'txn_id':txn_id,'works':works,'amount':order_obj.amount,'name':order_obj.name})

def become_volunteer(request):
    if request.method=="POST":
        status = ''
        name = request.POST['name']
        email = request.POST['email']
        contact = request.POST['contact']
        country = request.POST['country']
        state = request.POST['state']
        city = request.POST['city']
        address_line1 = request.POST['address1']
        address_line2 = request.POST['address2']
        age = request.POST['age']
        qual = request.POST['qualification']
        rem = request.POST['remarks']
        
        data = volunteer.objects.filter(contact=contact)
        if len(data)>=1:
            status = 'A Volunteer with this mobile number already exists!!!'
        else:
            data=volunteer(name=name,email=email,contact=contact,country=country,state=state,city=city,address_line1=address_line1,address_line2=address_line2,age=age,qualification=qual,other_information=rem,status=True)
            data.save()
            if 'profile_pic' in request.FILES:
                pp = request.FILES['profile_pic']
                data.profile_pic = pp
                data.save()
            status = "Dear {} welcome to Charity Hub, We will soon contact you if any charity event in your area".format(name)
            return render(request,'volunteer.html',{'status':status,'works':works})
            
    return render(request,'volunteer.html',{'works':works})
from django.db import models

# Create your models here.
class work(models.Model):
    title=models.CharField(max_length=200)
    description1 = models.TextField()
    description2 = models.TextField(blank=True)
    description3 = models.TextField(blank=True)
    added_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title

class workImages(models.Model):
    work = models.ForeignKey(work,on_delete=models.CASCADE)
    image1 = models.ImageField(upload_to="works/%Y/%m/%d",blank=True)
    image2 = models.ImageField(upload_to="works/%Y/%m/%d",blank=True)
    image3 = models.ImageField(upload_to="works/%Y/%m/%d",blank=True)
    image4 = models.ImageField(upload_to="works/%Y/%m/%d",blank=True)
    added_on = models.DateField(auto_now_add=True)
    def __str__(self):
        return self.work.title

class contactus(models.Model):
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    message = models.TextField()
    create_date = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.message
class donor(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200,blank=True)
    contact = models.IntegerField(null=True,blank=True)
    country = models.CharField(max_length=200,blank=True)
    state = models.CharField(max_length=200,blank=True)
    city = models.CharField(max_length=200,blank=True)
    address_line1 = models.CharField(max_length=500,blank=True)
    address_line2 = models.CharField(max_length=500,blank=True)
    amount = models.DecimalField(max_digits=10,decimal_places=2)
    remarks = models.TextField(blank=True)
    work = models.CharField(max_length=300,blank=True)
    payment_method=models.CharField(max_length=200,blank=True)
    txn_id = models.CharField(max_length=300,blank=True)
    on_date = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class volunteer(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200,blank=True)
    contact = models.IntegerField(null=True,blank=True)
    country = models.CharField(max_length=200,blank=True)
    state = models.CharField(max_length=200,blank=True)
    city = models.CharField(max_length=200,blank=True)
    address_line1 = models.CharField(max_length=500,blank=True)
    address_line2 = models.CharField(max_length=500,blank=True)
    age = models.IntegerField(null=True,blank=True)
    qualification = models.CharField(max_length=300,blank=True)
    profile_pic=models.ImageField(upload_to="volunteers/%Y/%m/%d")
    other_information = models.TextField(blank=True)
    on_date = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.name